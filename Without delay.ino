// Metronome without delay

#include <LiquidCrystal_I2C.h>
LiquidCrystal_I2C lcd(0x27, 16, 2);
const int Switch = 8;
const int Button = 10;
const int Pot = 2;
const int Speaker = 11;
int silenceduration;
int Pitch = 400; // in programm *10
int TIME = 0;
int setScreen;
int beepTime = 1;
int lastButtonState = 0;

void setup() {
  pinMode(Switch, INPUT);
  pinMode(Button, INPUT);
  pinMode(Pot, INPUT);
  pinMode(Speaker, OUTPUT);
  lcd.begin();
  lcd.backlight();
  lcd.home();
  lcd.print("Welcome by this");
  lcd.setCursor (0,1);
  lcd.print("metronome");
  delay(2000);
  lcd.clear();
}

void loop() {
  setScreen = 0;
  while (setScreen == 0) {
    silenceduration = analogRead(Pot);
    silenceduration = map(silenceduration, 0, 1023, 8, 80);
    if (digitalRead(Switch) == HIGH) {
      lcd.noBacklight();
      if (TIME == beepTime) {
        tone(Speaker, Pitch*10);
      }
      else if (TIME == beepTime+1) {
        noTone(Speaker);
      }
      else if (TIME >= silenceduration) {
      TIME = 0;
      }
      TIME += 1;
    }
  
    else {
      noTone(Speaker);
      lcd.backlight();
      if (digitalRead(Button) == HIGH && lastButtonState == 0) {
        lastButtonState = 1;
        lcd.clear();
        lcd.setCursor(1,0);
        lcd.print("Settings");
        lcd.setCursor(12,0);
        lcd.print("Exit");
        lcd.setCursor(1,1);
        lcd.print("Info");
        setScreen = 1;
        while (setScreen == 1) {
          lcd.setCursor(0,0);
          lcd.print(" ");
          lcd.setCursor(11,0);
          lcd.print(" ");
          lcd.setCursor(0,1);
          lcd.print(" ");
          if (analogRead(Pot) > 681) {  
            lcd.setCursor(0,0);
          }
          else if (analogRead(Pot) > 340 && analogRead(Pot) < 682) {
            lcd.setCursor(0,1);
          }
          else {
            lcd.setCursor(11,0);
          }
          lcd.write(126);
          delay(50);
            if (digitalRead(Button) == HIGH && lastButtonState == 0 && analogRead(Pot) > 681) {
              lastButtonState = 1;
              setScreen = 2;
              lcd.clear();
              while (setScreen == 2) {
                settings();  
              }
            }
            else if (digitalRead(Button) == HIGH && lastButtonState == 0 && analogRead(Pot) > 340 && analogRead(Pot) < 682) {
              lastButtonState = 1;
              setScreen = 2;
              lcd.clear();
              while (setScreen == 2) {
                info();
              }
            }
            else if (digitalRead(Button) == HIGH && lastButtonState == 0 && analogRead(Pot) < 341) {
              lastButtonState = 1;
              setScreen = 0;
              lcd.clear();
            }
            else if (digitalRead(Button) == LOW){
              lastButtonState = 0;
            }
        }
      }
      else if (digitalRead(Button) == LOW){
        lastButtonState = 0;
      }
      TIME = 0;
    }
    lcdwrite ();
  }
}

void lcdwrite() {
  lcd.home();
  lcd.print("Sound");
  lcd.setCursor (9,0);
  lcd.print("Speed");
  lcd.setCursor (9,1);
  lcd.print(3300/silenceduration);
  lcd.print(" b/m  ");
  lcd.setCursor (0,1);
  if (digitalRead(Switch) == HIGH) {
    lcd.print("On      ");
    setScreen = 0;
  }
  else {
    lcd.print("Off     ");
  }
}

void settings() {
  lcd.home();
  lcd.print(" Settings");
  lcd.setCursor(11,0);
  lcd.print(" Exit");
  lcd.setCursor(0,1);
  lcd.print(" Pitch");
  lcd.setCursor(12,1);
  lcd.print(Pitch*10);
  lcd.setCursor(11,1);
  lcd.print(" ");
  if (analogRead(Pot) > 512) {
    lcd.setCursor(0,1);
  }
  else {
    lcd.setCursor(11,0);
  }
  lcd.write(126);
  delay(100);
  if (digitalRead(Button) == HIGH && lastButtonState == 0 && analogRead(Pot) > 512) {
    lastButtonState = 1;
    setScreen = 3;
    while(setScreen == 3) {
      Pitch = analogRead(Pot);
      Pitch = map(Pitch, 0, 1023, 800, 1); 
      lcd.setCursor(12,1);
      lcd.print("    ");
      lcd.setCursor(12,1);
      lcd.print(Pitch*10);
      lcd.setCursor(11,1);
      lcd.write(126);
      delay(100);
      if (digitalRead(Button) == HIGH && lastButtonState == 0) {
        lastButtonState = 1;
        setScreen = 2;
      }
      else if (digitalRead(Button) == LOW){
        lastButtonState = 0;
      }
    }
  }
  else if (digitalRead(Button) == HIGH && lastButtonState == 0 && analogRead(Pot) < 512) {
    lastButtonState = 1;
    setScreen = 0;
    lcd.clear();
  }
  else if (digitalRead(Button) == LOW){
    lastButtonState = 0;
  }
}

void info() {
  lcd.home();
  lcd.print("Owner:");
  lcd.setCursor (0,1);
  lcd.print("Jantine Blok");
  delay(2000);
  lcd.clear();

  lcd.print("Made by:");
  lcd.setCursor (0,1);
  lcd.print("Kees Blok");
  delay(2000);
  lcd.clear();

  lcd.print("Gotten for your");
  lcd.setCursor (0,1);
  lcd.print("15th birthday");
  delay(2000);
  lcd.clear();

  setScreen = 0;
}